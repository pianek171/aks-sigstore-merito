# Sigstore on AKS Cluster

## Getting started

This IaC has been designed to generate AKS Cluster on Azure with integrated Sigstore to protect cluster against unsecure images.
Sigstore is used to sign the images and deploy admission controler to check the image signature.

![Environment Schema](diagram.png)

## Create AKS cluser via Terraform

Login to Azure
`az login`
`az login --tenant $ARM_TENANT_ID`  

Create service user  
```
export MSYS_NO_PATHCONV=1  
export ARM_SUBSCRIPTION_ID=$(az account subscription list | jq -r '.[] | .subscriptionId')
TECH_USER=techuser
az ad sp create-for-rbac --name $TECH_USER --role Contributor --scopes /subscriptions/$ARM_SUBSCRIPTION_ID > cred.json
```

Login as Service Principal
```
export ARM_TENANT_ID=$(jq -r '.tenant' cred.json)
export ARM_CLIENT_ID=$(jq -r '.appId' cred.json)
export ARM_CLIENT_SECRET=$(jq -r '.password' cred.json)
```
These values will be used by Terraform.

```
terraform init
terraform plan
terraform apply
```

Connect to the cluster  
``` 
export KUBECONFIG=$(pwd)/kubeconfig.yaml
kubectl get nodes
```
!!! TF VARS BACKEND FILE NOT ADDED / OUTPUT SHOULD BE REDIRECTED TO FILES


## Sigstore scenario 

### Generating keypair
Generate default keypair cosign.key(Private key) and cosign.pub(Public key)  
`cosign generate-key-pair`

### Signing the Image
Check the required RepoDigests
`docker inspect --type=image $IMAGE:$TAG | jq -r '.[] | .RepoDigests'`
Sign a container image with a local key pair file  
`cosign sign --key cosign.key $REPODIGESTS`  
Sign a container image using hashicorp vault  
`cosign sign --key hashivault://[KEY] $IMAGE:$TAG`  
Sign a container image with a key pair stored in Azure Key Vault  
`cosign sign --key azurekms://[VAULT_NAME][VAULT_URI]/[KEY] $IMAGE:$TAG`  
Push the image to repository  
`docker push repo.com/myrepo/$IMAGE:$TAG`  

### Verifying the Image locally
Pull the image  
`docker pull repo.com/myrepo/$IMAGE:$TAG`  
Verify signed image  
`cosign verify --key cosign.pub $IMAGE:$TAG`  

### Verifying the Image on K8S cluster using policy-controller (Admission controller)
Install the Helm Chart https://github.com/sigstore/helm-charts/tree/main/charts/policy-controller

policy-controller works by default only on namespaces with tag "policy.sigstore.dev/include=true"  
`kubectl label namespace my-secure-namespace policy.sigstore.dev/include=true`  

Prepare and apply admission controller config  
`kubectl apply -f admission.yml`

### Admission controller test
```
kubectl create secret docker-registry my-dockerhub-secret \
  --docker-username=your-dockerhub-username \
  --docker-password=your-dockerhub-password \
  --docker-email=your-email@example.com \
  --docker-server=https://index.docker.io/v2/ \
  -n my-secure-namespace

kubectl run nginx-pod --image=nginx:latest --port=80 -n my-secure-namespace
kubectl create -f signed_pod.yml

```