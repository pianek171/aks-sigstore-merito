variable "prefix" {
  description = "A prefix used for all resources in this example"
  default = "testowo"
}

variable "location" {
  description = "The Azure Region in which all resources in this example should be provisioned"
  default = "polandcentral"
}


variable "full_region" {
  description = "The Azure Region in which all resources in this example should be provisioned"
  default = "Poland Central"
}

